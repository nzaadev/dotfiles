# Install aria2 

```
sudo apt install aria2
```

lalu copy isi folder `.aria2` ke folder `~/.aria2` dan jalankan dengan perintah 

```
aria2c --daemon
```

config tadi akan termuat secara otomatis

untuk frontend aria2 bisa menggunakan aria2p yang berbasis CLI atau bisa menggunakan extensi Browser 

- Firefox: [Aria2 Integration](https://addons.mozilla.org/en-US/firefox/addon/aria2-integration/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
- Chrome: [Aria2 Explorer](https://chromewebstore.google.com/detail/aria2-explorer/mpkodccbngfoacfalldjimigbofkhgjn)



-
