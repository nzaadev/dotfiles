# dotfiles

My personal dot files


## Install tmux plugin manager (tpm)

```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

```

tambahkan ke `.tmux.conf`

```
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'github_username/plugin_name#branch'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
```

Reload tmux
```
tmux source ~/.tmux.conf
```
### Install plugin tmux

- tambahkan plugin ke `~/.tmux.conf` misal seperti ini `set -g @plugin '...'`
- install dengan `prefix` + `I` (i besar)

### hapus plugin tmux
- hapus plugin nya 
- tekan `prefix` + `alt` + `u` untuk update tmux plugin


## Sedikit tentang Tmux
- Session
Tmux bisa dibuat dengan banyak sesi, didalam sesi terdapat beberapa windows, dan di window terdapat pane. pada saat menjalankan perintah `tmux` saja secara otomatis tmux membuat sesi dengan nama 0, karena kita tidak memeberikan nama pada session tersebut

untuk memberikan nama bisa menggunakan option `-s` seperti ini `tmux -s nama_sesi` maka akan terbuka sesi tmux dengan nama `nama_sesi` selain itu tmux juga bisa di lepaskan dari layar atau istilah detach, dan menempelkan pada layar atau attach, untuk detach tmux bisa menngunakan `prefix` + `d` dan untuk attach tmux ke layar `tmux attach -t nama_sesi`

mengganti nama sesi tmux
`tmux rename-session -t nama_sesi nama_sesi_baru` atau bisa juga dari dalam sesi tmux dengan `prefix` + `$` lalu masukkan nama baru dan tekan `enter`

untuk melihat list sesi tmux bisa menggunakan perintah `tmux ls`

  - `prefix` + `$` mengganti nama sesi
  - `prefix` + `s` menampilkan daftar session
  - `prefix` + `w` menampilkan sesi dan window
  - `prefix` + `d` detach sesi
  - `prefix` + `(`  ke sesi sebelumnya
  - `prefix` + `)` ke sesi berikutnya 

- Window
Window adalah sebuah area yang menampilkan program atau shell yang sedang berjalan, kita bisa mengatur window untuk menambah, menghapus, menavigasi window dan sesi tmux

  - `prefix` + `c` membuat window baru
  - `prefix` + `&` kill jendela
  - `prefix` + `p` beralih ke jendela sebelumnya
  - `prefix` + `n` beralih jendela berikutnya
  - `prefix` + `w` memilih daftar window
  - `prefix` + `0` beralih ke window 0 (ganti 0 dengan nomer yang diinginkan)
  - `prefix` + `,` mengganti nama jendela
  - `prefix` + `number` berpindah ke window berdasarkan index
  - `prefix` + `l` ke jendela terakhir



- Pane
Pane adalah area yang menampilkan bagian dari program atau shell yang sedang berjalan dalam window, kita bisa membagi area terminal menjadi beberapa pane untuk menjalankan program atau shell secara bersamaan

  - `prefix` + `"` split horizontal
  - `prefix` + `%` split vertical
  - `prefix` + `x` kill pane
  - `prefix` + `o` beralih ke pane berikutnya
  - `prefix` + `;` beralih antar pane
  - `prefix` + `{` pindahkan pane ke kiri
  - `prefix` + `}` pindahkan pane ke kanan
  - `prefix` + `tombol arah panah` pindah ke pane sesuai arah 
  - `prefix` + `tambol arah (ctrl ditahan)` merubah ukuran pane
  - `prefix` + `spasi` berpindah layout pane antara horizontal dan vertikal
  - `prefix` + `q` menampilkan nomer index pane
  - `prefix` + `q`  + `nomer` berpindah pane berdasarkan nomer index
  - `prefix` + `z` zoom pane
  - `prefix` + `!` mengubah pane menjadi window


## Copy Paste di tmux

  - Tambahakn `setw -g mode-keys vi` pada `.tmux.conf`
  - tekan `prefix` + `[` untuk masuk dalam mode copy vi
  - arahkan dengan navigasi vim `hjkl` 
  - untuk memulai memilih teks tekan `spasi` lalu copy dengan `Enter`
  - untuk paste tekan `prefix` + `]`




[Referensi](https://tmuxcheatsheet.com)
