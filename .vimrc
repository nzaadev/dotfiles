syntax on
set number
set laststatus=2
set noshowmode

if !has('gui_running')
  set t_Co=256
endif

call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'joshdick/onedark.vim'
call plug#end()

" https://stackoverflow.com/a/50665913
colorscheme onedark


